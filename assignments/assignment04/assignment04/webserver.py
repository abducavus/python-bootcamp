
# https://fastapi.tiangolo.com/#installation


from typing import Annotated
import uuid
from fastapi import Body, FastAPI
from pydantic import BaseModel, Field

from . import sql_connection

app = FastAPI()

print(type(sql_connection))
cur = sql_connection.cursor()

class Tree():
    """
    create a Tree data model with pydantic and the below described Attributes

    Attributes
    ----------
    tree_id : uuid.UUID
        uuid of the UUID
    tree_name : str 
        the common name of the tree
    botanical_name : str | None
        botanical name of the tree 
        string with max 100 characters or None 
    age : int
        must be greater than 0
    """

    ...


@app.get("/")
def read_root():
    cur = sql_connection.cursor()
    data = {"treeId": str(uuid.uuid1()), "treeName": "Eiche", "botanicalName": "Quercus", "age": 99}
    cur.execute("INSERT INTO trees VALUES(:treeId, :treeName, :botanicalName, :age)", data)
    cur.execute("SELECT * FROM trees")
    print(cur.fetchall())
    
    return {"Hello": "World"}


def get_tree():
    """
    Expose this function as GET request with FastAPI under the path /tree

    Args:
        Query Paramters
            tree_id: uuid.UUID

    Returns
        Http Status Code
            200: if entry was found
            404: if the entry was not found
            400: else
        Body:
            the new Tree object as JSON
    """

    ...


def add_tree():
    """
    Expose this function as POST request with FastAPI under the path /tree
    Note: the tree_name is not required to be unique in the db

    Args:
        Query Paramters
            tree_id: uuid.UUID

    Returns
        Http Status Code
            200: if entry was successfully created
            400: else
        Body:
            the new Tree object as JSON
    """

    ...


def update_tree():
    """
    Expose this function as PUT request with FastAPI under the path /tree

    Args:
        Query Paramters
            tree_id: uuid.UUID
        Body:
            updated Tree object as JSON

    Returns
        Http Status Code
            200: if entry was successfully updated
            404: if the entry was not found in the database
            400: else
        Body:
            empty
    """

    ...


def delete_tree():
    """
    Expose this function as DELETE request with FastAPI under the path /tree

    Args:
        Query Paramters
            tree_id: uuid.UUID

    Returns:
        Http Status Code
            200: if entry was successfully deleted
            404: if the entry was not found in the database
            400: else
        Body:
            empty
    """
    
    ...
