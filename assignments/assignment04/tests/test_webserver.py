# -*- coding: utf-8 -*-

import pytest
import httpx
import uuid
from assignment04.webserver import get_tree, add_tree, update_tree, delete_tree

tree1 = {"treeId": str(uuid.uuid1()), "treeName": "Eiche",
         "botanicalName": "Quercus", "age": 99}
tree1_update = {"treeId": tree1['treeId'],
                "treeName": "Andere Eiche", "botanicalName": "Quercus", "age": 42}
tree2 = {"treeId": str(uuid.uuid1()), "treeName": "No one knows what it's like To be the bad man To be the sad man Behind blue eyes And no one knows what it's like To be hated To be fated to telling only lies",
         "botanicalName": "Quercus", "age": 99}
tree3 = {"treeId": str(uuid.uuid1()), "treeName": "Eiche",
         "botanicalName": "Quercus", "age": -5}


@pytest.mark.order(1)
def test_add_tree():
    r = httpx.post('http://localhost:8000/tree/', tree1)
    r.status_code == httpx.codes.OK
    r.json()['treeId'] == tree1['treeId']
    r.json()['treeName'] == tree1['treeName']
    r.json()['botanicalName'] == tree1['botanicalName']
    r.json()['age'] == tree1['age']


@pytest.mark.order(2)
def test_update_tree():
    r = httpx.put(f'http://localhost:8000/tree/{tree1['treeId']}', tree1_update)
    r.status_code == httpx.codes.OK


@pytest.mark.order(3)
def test_get_tree():
    r = httpx.get(f'http://localhost:8000/tree/{tree1['treeId']}')
    r.status_code == httpx.codes.OK
    r.json()['treeId'] == tree1_update['treeId']
    r.json()['treeName'] == tree1_update['treeName']
    r.json()['botanicalName'] == tree1_update['botanicalName']
    r.json()['age'] == tree1_update['age']


@pytest.mark.order(4)
def test_delete_tree():
    r = httpx.delete(f'http://localhost:8000/tree/{tree1['treeId']}')
    r.status_code == httpx.codes.OK


@pytest.mark.order(5)
def test_get_tree_not_found():
    r = httpx.get(f'http://localhost:8000/tree/{tree1['treeId']}')
    r.status_code == httpx.codes.NOT_FOUND


@pytest.mark.order(6)
def test_delete_tree_not_found():
    r = httpx.delete(f'http://localhost:8000/tree/{tree1['treeId']}')
    r.status_code == httpx.codes.NOT_FOUND


def test_add_tree_too_long_name():
    r = httpx.post('http://localhost:8000/tree/', tree2)
    r.status_code == httpx.codes.BAD_REQUEST


def test_add_tree_negative_age():
    r = httpx.post('http://localhost:8000/tree/', tree3)
    r.status_code == httpx.codes.BAD_REQUEST


# def test_test():
#     r = httpx.get('http://localhost:8000/')
#     r.status_code == httpx.codes.OK
#     r.text == "Hello World"