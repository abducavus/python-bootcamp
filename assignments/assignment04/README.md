# Webserver test

Write with fastAPI a simple RESTful webserver for to manage for a tree register
Use the skeleton in webserver.py
The database connection 'sql_connection' is given via an import. It is a sqlite database: https://docs.python.org/3/library/sqlite3.html 
The database runs in memory and is lost after the webserver terminates

Here is some example code how to use the sqlite adapter
```
cur = sql_connection.cursor()
data = {"treeId": str(uuid.uuid1()), "treeName": "Eiche", "botanicalName": "Quercus", "age": 99}
cur.execute("INSERT INTO trees VALUES(:treeId, :treeName, :botanicalName, :age)", data)
cur.execute("SELECT * FROM trees")
print(cur.fetchall())
```

Sidenote: noramlly the database would create the uuid but fore the sake of simplicity we generate the uuids in python

## Testing
To run the unittests for this assignment the webserver needs to be up and running first. Otherwise the unittests will fail.
Simply open two terminal sessions. Start the webserver with 'poetry run fastapi dev ./assignment04/webserver.py'. Then execute the pytest in the second terminal
Since no database cleanup was implemented, please start the webserver new to reset the database before running the unittest again.

### Testing hint
If you want to use the api spec of your webserver open the url in a private browser session to avoid caching problems