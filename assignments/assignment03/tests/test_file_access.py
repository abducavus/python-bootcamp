# -*- coding: utf-8 -*-

import assignment03.file_access as fa
from assignment03.file_access import csv_to_json, fun_with_paths, fun_with_paths_part2, implement_walk_function
from pathlib import Path
import pytest
import os


@pytest.fixture
def csv_to_json_prep():
    if Path(__file__, "../../data/userdata.json").is_file():
        Path(__file__, "../../data/userdata.json").unlink(missing_ok=False)
    yield None
    if Path(__file__, "../../data/userdata.json").is_file():
        Path(__file__, "../../data/userdata.json").unlink(missing_ok=False)


def test_csv_to_json(csv_to_json_prep):
    csv_to_json()
    assert Path(__file__, "../../data/userdata.json").is_file()
    f_solution = Path(__file__, "../data/userdata_testdata.json").read_text()
    f_result = Path(__file__, "../../data/userdata.json").read_text()
    assert f_solution == f_result


def test_fun_with_paths():
    moudle_path, lecture03_path_relative, lecture03_path_absolute = fun_with_paths()
    assert Path(fa.__file__).absolute() == moudle_path.absolut()
    assert str(Path("..\..\lectures\lecture03.pptx")
               ) == str(lecture03_path_relative)
    assert (moudle_path /
            lecture03_path_relative).resolve() == lecture03_path_absolute.resolve()


def test_fun_with_paths_part2():
    files_result, suffixes_result = fun_with_paths_part2()
    files_solution = list(map(lambda f: f,
                              filter(lambda f: f.suffix != ".py" and f.suffix != ".pyc", Path(__file__).parent.parent.rglob("*"))))
    suffixes_solution = set(
        filter(bool, map(lambda item: item.suffix, files_solution)))
    assert [map(str, suffixes_solution)].sort() == [
        map(str, suffixes_result)].sort()
    assert files_solution == files_result


def test_implement_walk_function():
    root_r, dirs_r, files_r = implement_walk_function(Path(__file__).parent)
    root_s, dirs_s, files_s = Path(__file__).parent.walk()
    assert root_r == root_s
    assert dirs_r == dirs_s
    assert files_r == files_s
