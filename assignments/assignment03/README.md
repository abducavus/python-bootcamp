# Assignment Tasks

- Write at least one unittest for each Task in king_cobra.py
- Implement the Tasks in file_access.py


## Optional Tasks
The optional task is marked in the descirption. Optional tasks have a higher difficulty but are not expected to be solved. 
Try them for fun
